const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const cors = require('cors');

server.listen(8000, () => console.log('connected to port 8000!'));

app.use(cors());

io.on('connection', (socket) => {
	socket.on('action', ({ type, payload }) => {
		switch (type) {
			case 'CHAT/ADD_MESSAGE':
				socket.broadcast.emit('action', { type: 'CHAT/NEW_MESSAGE', payload });
				break;
			default:
				console.error(`no case for type "${type}"`, payload);
		}
	});
});
