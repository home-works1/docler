import { applyMiddleware, compose, createStore } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import io from 'socket.io-client';
import createSocketIoMiddleware from 'redux-socket.io';
import logger from 'redux-logger';

import createRootReducer, { ApplicationState } from './reducers';
export const history = createBrowserHistory();

const socket = io('http://localhost:8000/');
const socketIoMiddleware = createSocketIoMiddleware(socket, 'CHAT/ADD_MESSAGE');

export default function configureStore() {
	const store = createStore(
		createRootReducer(history),
		compose(applyMiddleware(logger, socketIoMiddleware, routerMiddleware(history))),
	);
	return store;
}
