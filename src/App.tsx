import React, { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { MuiThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import SettingsIcon from '@material-ui/icons/Settings';
import CssBaseline from '@material-ui/core/CssBaseline';
import { SettingsState } from './routes/Settings/reducers';
import themes from './theme';
import { ChatState } from './routes/Chat/reducers';
import { ApplicationState } from './reducers';

const App = ({ theme, unread, children }: Props) => (
	<MuiThemeProvider theme={themes[theme]}>
		<CssBaseline />
		<AppBar position="sticky" color="inherit">
			<Toolbar>
				<Link to="/">
					<IconButton color="primary">
						<Badge badgeContent={unread} color="secondary">
							<MailIcon />
						</Badge>
					</IconButton>
				</Link>
				<Link to="/settings">
					<IconButton color="primary">
						<SettingsIcon />
					</IconButton>
				</Link>
			</Toolbar>
		</AppBar>
		{children}
	</MuiThemeProvider>
);

function mapStateToProps({ settings: { theme }, chat: { unread } }: ApplicationState) {
	return {
		theme,
		unread,
	};
}

export default connect(mapStateToProps)(App);

interface Props {
	theme: SettingsState['theme'];
	unread: ChatState['unread'];
	children: ReactNode;
}
