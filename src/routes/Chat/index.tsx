import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';

import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Message from '../../component/Message';
import AddMessageForm from '../../component/Form';
import { addMessage, scrollToBottom, ChatActions } from './actions';
import { ChatState, ChatMessage } from './reducers';
import { ApplicationState } from '../../reducers';
import { SettingsState } from '../Settings/reducers';

const ChatRoute = ({ chat: { messages }, settings, addMessage, scrollToBottom, classes }: Props) => {
	const formSubmit = (message: ChatMessage) => {
		addMessage(message);
	};
	useEffect(() => {
		scrollToBottom(window.outerHeight);
	});
	return (
		<Fragment>
			<List className={classes.list} disablePadding>
				{messages.map((message) => <Message key={message.id} message={message} settings={settings} />)}
			</List>
			<AppBar position="fixed" color="inherit" className={classes.appBar}>
				<Toolbar>
					<AddMessageForm addMessage={formSubmit} settings={settings} />
				</Toolbar>
			</AppBar>
		</Fragment>
	);
};
const styles = (theme: Theme) => ({
	list: {
		marginBottom: '3.5em',
		backgroundColor: theme.palette.background.paper,
	},
	appBar: { top: 'auto', bottom: 0 },
});
export default connect(({ chat, settings }: ApplicationState) => ({ chat, settings }), {
	addMessage,
	scrollToBottom,
})(withStyles(styles)(ChatRoute));

interface Props extends ChatActions, WithStyles<typeof styles> {
	chat: ChatState;
	settings: SettingsState;
}
