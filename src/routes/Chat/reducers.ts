let isChatting: boolean = true;
let clearBlinkingTab = () => {};
const defaultState = { messages: [], unread: 0 };
export default function chat(state = defaultState as ChatState, action: AnyAction) {
	const message = action.payload;
	const messages = state.messages.concat(message);
	switch (action.type) {
		case 'CHAT/NEW_MESSAGE':
			let unread = state.unread + 1;
			if (unread > 0) {
				clearBlinkingTab = blinkTab(`(${unread}) unread message(s)`);
			}
			if (isChatting) {
				unread = 0;
			}
			return { ...state, unread, messages };
		case 'CHAT/ADD_MESSAGE':
			return { ...state, messages };
		case '@@router/LOCATION_CHANGE':
			isChatting = action.payload.location.pathname === '/';
			if (isChatting) {
				clearBlinkingTab();
				return { ...state, unread: 0 };
			}
			return state;
		case 'CHAT/SCROLL':
			window.scroll(0, action.payload);
			return state;
		default:
			return state;
	}
}

function blinkTab(message: string, ms = 1000) {
	let timeoutId: any;
	const oldTitle = document.title;
	const handleVisibilityChange = () => {
		if (document.hidden) {
			blink();
		} else {
			clear();
		}
	};
	const blink = () => (document.title = document.title === message ? ' ' : message);
	const clear = () => {
		clearInterval(timeoutId);
		document.title = oldTitle;
		timeoutId = 0;
		document.removeEventListener('visibilitychange', handleVisibilityChange, false);
	};
	if (!timeoutId) {
		timeoutId = setInterval(blink, ms);
		document.addEventListener('visibilitychange', handleVisibilityChange, false);
	}
	return clear;
}
export interface ChatState {
	messages: ChatMessage[];
	unread: number;
}
export interface ChatMessage {
	author: string;
	id: string;
	text: string[];
	date: number;
}
