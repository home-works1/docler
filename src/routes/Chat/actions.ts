import { ChatMessage } from './reducers';

export const addMessage = (message: ChatMessage) => {
	return {
		type: 'CHAT/ADD_MESSAGE',
		payload: message,
	};
};
export const scrollToBottom = (bottom: number) => {
	return {
		type: 'CHAT/SCROLL',
		payload: bottom,
	};
};

export interface ChatActions {
	addMessage(message: ChatMessage): void;
	scrollToBottom(bottom: number): void;
}
