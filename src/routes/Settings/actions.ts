import { SettingsState } from './reducers';
export const saveSettings = (settings: Partial<SettingsState>) => ({
	type: 'SETTINGS/SAVE',
	payload: settings,
});

export interface SettingsActions {
	saveSettings({}): void;
}
