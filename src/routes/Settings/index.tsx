import React, { BaseSyntheticEvent } from 'react';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import Radio from '@material-ui/core/Radio';
import InputLabel from '@material-ui/core/InputLabel';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';

import { saveSettings, SettingsActions } from './actions';
import { defaults, SettingsState } from './reducers';
import { ApplicationState } from '../../reducers';

const SettingsRoute = ({ username, theme, timeFormat, sendOn, language, saveSettings, classes }: Props) => {
	const onChange = ({ target: { name, value } }: BaseSyntheticEvent) => {
		saveSettings({
			[name]: value,
		});
	};
	const onReset = () => {
		saveSettings(defaults);
	};
	return (
		<form onChange={onChange} onReset={onReset}>
			<List disablePadding>
				<ListItem>
					<TextField name="username" label="User name" margin="normal" defaultValue={username} placeholder="guest" />
				</ListItem>
				<ListItem>
					<FormControl>
						<FormLabel>Interface color</FormLabel>
						<RadioGroup row aria-label="Theme" name="theme" defaultValue={theme}>
							<FormControlLabel value="light" control={<Radio />} label="Light" />
							<FormControlLabel value="dark" control={<Radio />} label="Dark" />
						</RadioGroup>
					</FormControl>
				</ListItem>
				<ListItem>
					<FormControl>
						<FormLabel>Clock display</FormLabel>
						<RadioGroup row aria-label="Clock display" name="timeFormat" defaultValue={timeFormat}>
							<FormControlLabel value="12" control={<Radio />} label="12 hours" />
							<FormControlLabel value="24" control={<Radio />} label="24 hours" />
						</RadioGroup>
					</FormControl>
				</ListItem>
				<ListItem>
					<FormControl>
						<FormLabel>Send messages on CTRL+ENTER</FormLabel>
						<RadioGroup row aria-label="Send messages on CTRL+ENTER" name="sendOn" defaultValue={sendOn}>
							<FormControlLabel value="ctrl+enter" control={<Radio />} label="On" />
							<FormControlLabel value="enter" control={<Radio />} label="Off" />
						</RadioGroup>
					</FormControl>
				</ListItem>
				<ListItem>
					<FormControl>
						<InputLabel htmlFor="age-native-helper">Language</InputLabel>
						<NativeSelect name="language" defaultValue={language}>
							<option value="english">English</option>
						</NativeSelect>
					</FormControl>
				</ListItem>
			</List>
			<AppBar position="fixed" color="inherit" className={classes.appBar}>
				<Toolbar>
					<Button variant="contained" color="secondary" type="reset" fullWidth>
						<ThreeSixtyIcon />
						Reset to defaults
					</Button>
				</Toolbar>
			</AppBar>
		</form>
	);
};
const styles = () => ({
	appBar: { top: 'auto', bottom: 0 },
});

export default connect(({ settings }: ApplicationState) => settings, { saveSettings })(
	withStyles(styles)(SettingsRoute),
);

interface Props extends SettingsState, SettingsActions, WithStyles<typeof styles> {}
