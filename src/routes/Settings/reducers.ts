import Chance from 'chance';

const nameGetter = new Chance();

export const defaults = {
	username: nameGetter.first(),
	theme: 'light',
	timeFormat: '12',
	sendOn: 'enter',
	language: 'english',
};
const stored = getFromLocalStorage();

export default function settings(state: SettingsState = stored || defaults, action: AnyAction) {
	switch (action.type) {
		case 'SETTINGS/SAVE':
			let newSettings = { ...state, ...action.payload };
			saveToLocalStorage(newSettings);
			return newSettings;
		default:
			return state;
	}
}

function saveToLocalStorage(settings: SettingsState) {
	try {
		localStorage.setItem('settings', JSON.stringify(settings));
	} catch (e) {
		console.error('failed to save settings', e);
	}
}

function getFromLocalStorage(): any | undefined {
	try {
		const settings = localStorage.getItem('settings');
		if (settings) {
			return JSON.parse(settings);
		}
	} catch (e) {
		console.error('failed to restore user settings', e);
	}
}
export interface SettingsState {
	username: string;
	theme: 'light' | 'dark';
	timeFormat: '12' | '24';
	sendOn: 'enter' | 'ctrl+enter';
	language: 'english';
}
