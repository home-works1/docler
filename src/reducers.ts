import { combineReducers } from 'redux';
import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';
import settings, { SettingsState } from './routes/Settings/reducers';
import chat, { ChatState } from './routes/Chat/reducers';

const rootReducer = (history: History) =>
	combineReducers({
		chat,
		settings,
		router: connectRouter(history),
	});

export default rootReducer;
export interface ApplicationState {
	chat: ChatState;
	settings: SettingsState;
	router: RouterState;
}
