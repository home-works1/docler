import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';
import { createMuiTheme } from '@material-ui/core/styles';
const light = createMuiTheme({
	palette: {
		type: 'light',
	},
	typography: {
		useNextVariants: true,
	},
});
const dark = createMuiTheme({
	palette: {
		primary: {
			light: cyan[300],
			main: cyan[500],
			dark: cyan[700],
		},
		secondary: {
			light: green[300],
			main: green[500],
			dark: green[700],
		},
		type: 'dark',
	},
	typography: {
		useNextVariants: true,
	},
});

export default {
	light,
	dark,
};
