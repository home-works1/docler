import React from 'react';
import ReactDOM from 'react-dom';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';

import configureStore, { history } from './configureStore';
import App from './App';
import Chat from './routes/Chat';
import Settings from './routes/Settings';
import * as serviceWorker from './serviceWorker';

import './index.css';

const store = configureStore();

ReactDOM.render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<App>
				<Route exact path="/" component={Chat} />
				<Route exact path="/settings" component={Settings} />
			</App>
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
