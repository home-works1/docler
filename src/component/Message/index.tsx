import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import { Typography } from '@material-ui/core';
import { ChatMessage } from '../../routes/Chat/reducers';
import { SettingsState } from '../../routes/Settings/reducers';

function Message({ message: { author, date, text }, settings: { timeFormat, username } }: Props) {
	const textAlign = author === username ? 'right' : 'left';
	const avatar = author === username ? '' : <Avatar>{author.slice(0, 2)}</Avatar>;
	const messageDate = new Date(date);
	const now = new Date();
	const isToday = now.getTime() - messageDate.getTime() < 86400000;
	const options = { hour12: timeFormat === '12' };
	const displayTime = messageDate[isToday ? 'toLocaleTimeString' : 'toLocaleString']('en-US', options);
	return (
		<ListItem dense divider style={{ textAlign }}>
			{avatar}
			<ListItemText primary={author === username ? '' : author} secondary={displayTime} />
			<Typography dangerouslySetInnerHTML={{ __html: text.map(escapeHTML).join('<br />') }} />
		</ListItem>
	);
}
export default Message;

function escapeHTML(html: string) {
	return html.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

interface Props {
	settings: SettingsState;
	message: ChatMessage;
}
