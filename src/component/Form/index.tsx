import React, { BaseSyntheticEvent, KeyboardEvent } from 'react';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SubmitIcon from '@material-ui/icons/Send';
import { SettingsState } from '../../routes/Settings/reducers';

function AddMessageForm({ addMessage, settings: { username, sendOn } }: Props) {
	const submitHandler = (e: BaseSyntheticEvent) => addMessage(onSubmit(e));
	return (
		<form onSubmit={submitHandler}>
			<Input
				placeholder="Enter message"
				name="message"
				fullWidth
				required
				onKeyUp={maybeSubmit}
				onKeyDown={fixNewLine}
				multiline
				endAdornment={
					<InputAdornment position="end">
						<IconButton color="primary" type="submit" aria-label="Submit a message">
							<SubmitIcon />
						</IconButton>
					</InputAdornment>
				}
			/>
		</form>
	);
	function onSubmit(e: BaseSyntheticEvent) {
		const form = e.target;
		const text = form.message.value.split('\n');
		const author = username;
		const date = Date.now();
		const id = Math.random().toString(36).slice(6) + date.toString(36).slice(-2);
		form.reset();
		fixTextareaHeight(form.message);
		e.preventDefault();
		return {
			author,
			id,
			text,
			date,
		};
	}
	function fixTextareaHeight(textarea: HTMLTextAreaElement) {
		textarea.dispatchEvent(new Event('change', { bubbles: true }));
	}
	function maybeSubmit(e: KeyboardEvent<HTMLTextAreaElement>) {
		const { key, currentTarget, ctrlKey } = e;
		const form = currentTarget.form;
		if (key === 'Enter') {
			if ((sendOn === 'enter' && !ctrlKey) || (sendOn === 'ctrl+enter' && ctrlKey)) {
				if (currentTarget.value.length > 0 && form) {
					form.dispatchEvent(new Event('submit'));
				}
				e.preventDefault();
			}
		}
	}
	function fixNewLine(e: KeyboardEvent<HTMLTextAreaElement>) {
		const { key, currentTarget, ctrlKey } = e;
		if (key === 'Enter' && ctrlKey && sendOn === 'enter') {
			currentTarget.value += '\n';
			fixTextareaHeight(currentTarget); // does not work :sad_pepe:
		}
	}
}

export default AddMessageForm;

interface Props {
	addMessage({}): void;
	settings: SettingsState;
}
